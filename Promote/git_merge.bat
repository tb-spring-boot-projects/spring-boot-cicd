@echo off

SET VERSION=%1
SET GIT_URL=%2
SET PROJECT_PATH=%3
SET CI_PROJECT_ID=%4
SET API_TOKEN=%5
SET CI_ENV=%6
SET CI_BUILD_REF_NAME=%7
SET GITLAB_USER_NAME=%8


git remote update
echo "fetch origin RELEASE"
git fetch http://%GITLAB_USER_NAME%:%API_TOKEN%@gitlab.com%PROJECT_PATH% %CI_ENV%
echo "Merge RELEASE into RC keeping RC changes"
git pull http://%GITLAB_USER_NAME%:%API_TOKEN%@gitlab.com%PROJECT_PATH% RELEASE
echo "Git checkout RELEASE"
git checkout %CI_ENV%
echo "Merge RC into RELEASE forcing our changes"
git merge -m "%CI_BUILD_REF_NAME% into %CI_ENV%" %CI_BUILD_REF_NAME%
echo "Tag RELEASE with version number"
for /f %%i in ('echo %CI_BUILD_REF_NAME% ^| sed "s/rc/release/"') do set RELEASE_TAG=%%i
git tag %RELEASE_TAG%
git push http://%GITLAB_USER_NAME%:%API_TOKEN%@gitlab.com%PROJECT_PATH% %RELEASE_TAG%
echo "Git push newly merged RELEASE branch"
git push http://%GITLAB_USER_NAME%:%API_TOKEN%@gitlab.com%PROJECT_PATH% %CI_ENV% 
