@echo off

echo "# # # # # PROMOTING TO RELEASE CANDIDATE # # # # #"

SET API_TOKEN=%1
SET PROJECT_PATH=%2
set ORIGIN=http://tebriggs94:%API_TOKEN%@gitlab.com%PROJECT_PATH%

:: git checkout master
git checkout master
git fetch origin
git pull origin master

::for /f %%i in ('mvn help:evaluate -Dexpression=project.version -q -DforceStdout') do set 
::set CURRENT_VERSION=`mvn help:evaluate -Dexpression=project.version -q -DforceStdout`
call mvn help:evaluate -Dexpression=project.version -q -DforceStdout >> version.txt 
for /F "tokens=*" %%a in (version.txt) do set CURRENT_VERSION=%%a
:: SET CURRENT_TAG_VERSION = 'git tag -l "v%VERSION%.[0-9]*" v:refname | tail -1'
echo "CURRENT_VERSION: %CURRENT_VERSION%"

set CURRENT_MAJOR_MINOR_VERSION=`echo ${CURRENT_VERSION} | sed "s/-SNAPSHOT//"`
for /f %%i in ('echo %CURRENT_VERSION% ^| sed "s/-SNAPSHOT//"') do set CURRENT_MAJOR_MINOR_VERSION=%%i
echo "CURRENT_MAJOR_MINOR_VERSION: " %CURRENT_MAJOR_MINOR_VERSION%


for /f %%i in ('git tag -l ""v%CURRENT_MAJOR_MINOR_VERSION%.[0-9]*"" v:refname ^| tail -1') do set CURRENT_TAG_VERSION=%%i
:: SET CURRENT_TAG_VERSION = 'git tag -l "v%VERSION%.[0-9]*" v:refname | tail -1'
echo "CURRENT_TAG_VERSION: %CURRENT_TAG_VERSION%"

SET NEW_PATCH_VERSION=0
if "%CURRENT_TAG_VERSION%"=="" GOTO SKIP_GET_PATCH    
for /f %%i in ('echo %CURRENT_TAG_VERSION% ^| sed -e "s/v[0-9]\+\.[0-9]\+\.[0-9]\+\.//g" ') do set CURRENT_PATCH_VERSION=%%i
echo "CURRENT_PATCH_VERSION=%CURRENT_PATCH_VERSION%"
SET /A NEW_PATCH_VERSION = %CURRENT_PATCH_VERSION%+1

:SKIP_GET_PATCH

echo "NEW_PATCH_VERSION=%NEW_PATCH_VERSION%"
SET CI_NEW_VERSION=%CURRENT_MAJOR_MINOR_VERSION%.%NEW_PATCH_VERSION%
echo "CI_NEW_VERSION=%CI_NEW_VERSION%"
SET CI_NEW_BRANCH=rc-%CI_NEW_VERSION%
echo "CI_NEW_BRANCH=%CI_NEW_BRANCH%"


:: TAG MASTER WITH VERSION NUMBER
git tag v%CI_NEW_VERSION%
git push %ORIGIN% v%CI_NEW_VERSION%

:: CREATE RC BRANCH AND PUSH
echo "Checking out new branch - %CI_NEW_BRANCH%"
git checkout -b %CI_NEW_BRANCH%
:: echo "Setting new version"
call mvn -q versions:set -DnewVersion=%CI_NEW_VERSION% -DgenerateBackupPoms=false
find . -iname pom.xml -exec git add {} \;
:: Change version in project
echo "Commit branch"
git commit -am "Release branch %CI_NEW_BRANCH%"
echo "Pushing changes"
git push -q --set-upstream %ORIGIN% %CI_NEW_BRANCH%

:: git checkout master
git checkout master